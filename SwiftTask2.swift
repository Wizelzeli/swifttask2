// Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
let daysInMonths = [
31, 28, 31, 30, 
31, 30, 31, 31, 
30, 31, 30, 31
]
// Создать массив элементов 'название месяцов' содержащий названия месяцев
let monthNames = [
"January", "February", "March", "April", 
"May", "June", "July", "August", 
"September", "October", "November", "December"
 ]
// Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)
for days in daysInMonths {
	print(days)
}
// Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
for i in 0..<monthNames.count {
	print("\(monthNames[i]) - \(daysInMonths[i])")
}
// Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
let monthsTuple: [(name: String, days: Int)] = Array(zip(monthNames, daysInMonths))
for month in monthsTuple {
    print("\(month.name) -- \(month.days)")
}
// Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)
for (index, month) in monthsTuple.enumerated() {
  print("\(month.name) --- \(daysInMonths[daysInMonths.count - 1 - index])")
}
// Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года
let rMonth = Int.random(in: 0..<monthNames.count)
let rDay = Int.random(in: 1...daysInMonths[rMonth])

var resultSum = 0

for i in 0..<rMonth {
    resultSum += daysInMonths[i]
}
resultSum += rDay - 1

print("\(monthNames[rMonth]), \(rDay) - \(resultSum)")
